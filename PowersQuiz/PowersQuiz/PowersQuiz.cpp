//Stephen Goeddel
//Quiz on exponents of 2
//Sept '11
#include <iostream>
#include <ctime>
#include <cstring>
using namespace std;

int main()
{
	time_t StartTime = time(0);
	int A[50];
	int Penalties = 0;
	int right = 1;
	char UserAnswer[10];
	char Extension[5][2] = {"","k", "m", "b", "t"};
	char Powers[10][4] = {"1", "2", "4", "8", "16", "32", "64", "128", "256", "512"};
	char TrueAnswer[10];

	srand((unsigned int)time(0));
	for(int i = 0; i < 50; i++)//fills the array with numbers 0 to 49 
	{ 
		A[i] = i;
	}
	for(int i = 0; i < 50; i++)//shuffles the array
	{
		int r = i + (rand() % (50-i));
		int temp = A[i]; 
		A[i] = A[r];  
		A[r] = temp;

	}
	for (int j=0; j<50; j++)
	{
		//cout << A[j] << endl;
		int ones = A[j]%10;//computes the ones place
		//cout << ones << endl;
		int tens = A[j]/10;//computes the tens place
		//cout << tens << endl;
		strcpy(TrueAnswer, Powers[ones]);//copies the ones place to the true answer string
		strcat(TrueAnswer, Extension[tens]);// adds the tens place to the string
		//cout << TrueAnswer << endl;
		do{
			right = 1;
			cout << "what is 2 to the " << A[j] << " power?" << endl; 
			cin >> UserAnswer;

			if(strcmp(TrueAnswer, UserAnswer) ==0)// comparing the users answer to the correct answer
			{
				cout << "That is correct!" << endl;
				right = 0;
			}
			else if(strcmp(TrueAnswer, UserAnswer) > 0)
			{
				cout << "the correct answer is larger than your answer, please try again" << endl;
				Penalties +=5;
				
			}
			else
			{
				cout << "the correct answer is smaller than your answer" << endl;
				Penalties +=5;
				
			}
		
		}while(right !=0);
		
	}
	time_t EndTime = time(0);
	cout <<EndTime - StartTime << " Was your time" << endl;
	cout << "you incured " << Penalties << " seconds in penalties" << endl; 
	cout << EndTime - StartTime + Penalties << " Was your time with penalties included" << endl;

	system("PAUSE");
	return 0;
		}
	