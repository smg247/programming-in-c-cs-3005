// CS 3600 Graphics Programming
// Spring, 2009
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to a Windows Console project
// and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.

#include <cmath>
#include <cstring>
#include "glut.h"


// Global Variables (Only what you need!)
GLdouble screen_x = 700;
GLdouble screen_y = 500;


// 
// Functions that draw basic primitives
//
void DrawCircle(GLdouble x1, GLdouble y1, GLdouble radius)
{
	glBegin(GL_POLYGON);
	
	for(int i=0; i<32; i++)
	{
		double theta = (double)i/32.0 * 2.0 * 3.1415926;
		double x = x1 + radius * cos(theta);
		double y = y1 + radius * sin(theta);
		glVertex2d(x, y);
	}
	glEnd();
}

void DrawRectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)//top left, top right, bottom right, bottom left
{
	glColor3f (0, 0, 0);
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glVertex2d(x4,y4);
	glEnd();
	glColor3f (1, 0, 0);
	glBegin(GL_LINES);
	glVertex2d(x1, y1);             
	glVertex2d(x2, y2);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glVertex2d(x3,y3);
	glVertex2d(x4,y4);
	glVertex2d(x4,y4);
	glVertex2d(x1, y1);                 
	glEnd();    
}

void DrawTriangle(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2, GLdouble x3, GLdouble y3)
{
	glBegin(GL_TRIANGLES);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glEnd();
}

// Outputs a string of text at the specified location.
void text_output(GLdouble x, GLdouble y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
	
	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

    glDisable(GL_BLEND);
}


//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	//int x1,y1,x2,y2,x3,y3,x4,y4;
	double x1 = 10;
	double y1 = 10;
	double x2 = 70;
	double y2 = 10;
	double x3 = 70;
	double y3 = 70;
	double x4 = 10;
	double y4 = 70;
	double y1Loop = 10;
	double x1Loop = 10;
	glClear(GL_COLOR_BUFFER_BIT);
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{

			DrawRectangle(x1,y1,x2,y2,x3,y3,x4,y4);

			x1+=60;
			x2+=60;
			x3+=60;
			x4+=60;

		}
		x1=10;
		x2=70;
		x3=70;
		x4=10;
		y1+=60;
		y2+=60;
		y3+=60;
		y4+=60;
	}
		x1=40;
		x2=70;
		x3=70;
		x4=10;
		y1-= 30;
		y1Loop = y1;
		x1Loop = x1;
		
		y1-=60;
		glColor3f(1,0,0);
		for(int i = 0;i<8;i++)
		{
		DrawCircle(x1,y1Loop,15);
		y1Loop-=60;
		}
		y1+=60;
		y1Loop = y1;
		for(int i = 0;i<8;i++)
		{
		DrawCircle(x1Loop,y1,15);
		x1Loop+=60;
		}
		x1Loop = x1;
		for(int i = 0; i <8;i++)
		{
			DrawCircle(x1Loop,y1Loop,15);
			x1Loop +=60;
			y1Loop -=60;
		}
		glColor3f(0,0,1);
		DrawCircle(x1,y1,29);

	glutSwapBuffers();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
		case 27: // escape character means to quit the program
			exit(0);
			break;
		case 'b':
			// do something when 'b' character is hit.
			break;
		default:
			return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	glutPostRedisplay();
}

// Your initialization code goes here.
void InitializeMyStuff()
{
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Program 1 - Shapes");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);

	glColor3d(0,0,0); // forground color
	glClearColor(1, 1, 1, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
