#include <fstream>
#include "Shapes.h"
#include "glut.h"
#include "Graphics.h"
Shape::Shape()
{
}
void Shape::Save(ofstream& fout)
{
	fout<<" "<<mR<<" "<<mG<<" "<<mB<<endl;
}
Rectangle::Rectangle(double x1, double y1, double x2, double y2, double r, double g, double b)
{
	mX1 = x1;
	mY1 = y1;
	mX2 = x2;
	mY2 = y2;
	mR = r;
	mG = g;
	mB = b;
}
	
void Rectangle::Draw()
{
	glColor3d(mR, mG, mB);
	DrawRectangle(mX1, mY1, mX2, mY2);

}
void Rectangle::Save(ofstream& fout)
{
	fout<< "Rectangle " <<mX1 <<" "<< mY1 <<" "<< mX2<<" "<<mY2;
	Shape::Save(fout);
}
Rectangle::Rectangle()
{
	mX1 = 0;
	mY1 = 0;
	mX2 = 0;
	mY2 = 0;
	mR = 0;
	mG = 0;
	mB = 0;
}

Circle::Circle(double x, double y, double radious, double r, double g, double b)
{
	mX = x;
	mY = y;
	mR = r;
	mG = g;
	mB = b;
	mRadious = radious;
}
Circle::Circle()
{
	mX = 0;
	mY = 0;
	mR = 0;
	mG = 0;
	mB = 0;
	mRadious = 0;
}

void Circle::Draw()
{
	glColor3d(mR, mG, mB);
	DrawCircle(mX, mY, mRadious);
}
void Circle::Save(ofstream& fout)
{
	fout<<"Circle "<< mX <<" "<< mY <<" "<< mRadious<<" ";
	Shape::Save(fout);
}

Triangle::Triangle(double x1, double y1, double x2, double y2, double x3, double y3, double r, double g, double b)
{
	mX1 = x1;
	mY1 = y1;
	mX2 = x2;
	mY2 = y2;
	mX3 = x3;
	mY3 = y3;
	mR = r;
	mG = g;
	mB = b;
}
	
void Triangle::Draw()
{
	glColor3d(mR, mG, mB);
	DrawTriangle(mX1, mY1, mX2, mY2, mX3, mY3);

}
void Triangle::Save(ofstream& fout)
{
	fout<<"Triangle "<< mX1 <<" "<< mY1 <<" "<< mX2  <<" "<< mY2 <<" "<< mX3<<" "<< mY3;
	Shape::Save(fout);
}
Triangle::Triangle()
{
	mX1 = 0;
	mY1 = 0;
	mX2 = 0;
	mY2 = 0;
	mX3 = 0;
	mY3 = 0;
	mR = 0;
	mG = 0;
	mB = 0;
}
//make circle and triangle, dont forget to put them in the header.