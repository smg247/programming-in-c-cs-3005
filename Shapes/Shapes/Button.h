class Button
{
private: 
	char mLabel[100];
	int mX1, mY1, mX2, mY2;
	bool mSelected;
public://implement these 5
	Button(char label[], int x1, int y1, int x2,int y2, bool selected);
	bool IsSelected();
	void SetSelected(bool selected);
	void Draw();//use open gl commands to make a box and draw something inside it
	bool isClicked(int x, int y);
};