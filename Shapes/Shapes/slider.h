// Slider class
// By Bart Stander
// February 6, 2008
// For CS 1410
// This class if for maintaining a slider bar in OpenGL.
// It remebers its own position, color, and value.
// The value is always between 0 and 1.
// Calling HandleMouseClick will update the slider's value, if the x,y are inside the slider's boundaries.
// In that case it returns true.
#include "glut.h"
#include "Graphics.h"
class Slider
{
public:
	Slider(int left, int top, int right, int bottom, 
		double red, double green, double blue, double value)
	{
		mLeft = left;
		mTop = top;
		mRight = right;
		mBottom = bottom;
		mRed = red;
		mGreen = green;
		mBlue = blue;
		mValue = value;
	}

	void Draw()
	{
		// Draw the inside
		glColor3d(mRed, mGreen, mBlue);
		int right = mLeft + (mRight-mLeft)*mValue;
		DrawRectangle(mLeft, mTop, right, mBottom);
		// draw the outline:
		glColor3d(0,0,0);
		glBegin(GL_LINE_LOOP);
		glVertex2i(mLeft, mTop);
		glVertex2i(mRight, mTop);
		glVertex2i(mRight, mBottom);
		glVertex2i(mLeft, mBottom);
		glEnd();
	}
	bool HandleMouseClick(int x, int y) // resets the value
	{
		if(x>= mLeft && x<=mRight && y>=mBottom && y<=mTop)
		{
			mValue = (double)(x-mLeft)/(mRight-mLeft);
			return true;
		}
		else
		{
			return false;
		}
	}
	double GetValue()
	{
		return mValue;
	}
private:
	int mLeft, mTop, mRight, mBottom;
	double mRed, mGreen, mBlue;
	double mValue;
};
