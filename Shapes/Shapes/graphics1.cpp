// CS 3600 Graphics Programming
// Spring, 2009
//
// This is a starting point for OpenGl applications.
// Add code to the "display" function below, or otherwise
// modify this file to get your desired results.
//
// For the first assignment, add this file to a Windows Console project
// and then compile and run it as is.
// NOTE: You should also have glut.h,
// glut32.dll, and glut32.lib in the directory of your project.
// Stephen Goeddel, Novemeber 2011
#include <cmath>
#include <cstring>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;
#include "glut.h"
#include "Graphics.h"
#include "Button.h"
#include "Shapes.h"
#include "slider.h"


// Global Variables (Only what you need!)
GLdouble screen_x = 700;
GLdouble screen_y = 500;

double gTempClicksX[10];
double gTempClicksY[10];
int gTempClicksCount = 0;
bool help = false;
int helpCount = 2;
Slider gRedSlider(10, 100, 120, 70, 1, 0, 0, .75);//higher Y's first on sliders
Slider gGreenSlider(130, 100, 240, 70, 0, 1, 0, .75);
Slider gBlueSlider(250, 100, 360, 70, 0, 0, 1, .75);

vector<Shape*> gShapes;//vector of pointers to the base class Shapes.
Rectangle gRefPoint = Rectangle(0,0,2,2,1,1,1);
Rectangle gRefPoint1 = Rectangle(0,0,2,2,1,1,1);
Rectangle gbottom = Rectangle(0,150,700,0,.1,.1,.1);
Rectangle gColorBox = Rectangle(370,70,480,140,.75,.75,.75);
Button gRectButton = Button("Rectangle", 10,10,120,60,true);
Button gCircButton = Button("Circle", 130,10,240,60,false);
Button gTriButton = Button("Triangle", 250,10,360,60,false);
Button gClearButton = Button("Clear", 370,10,480,60,false);
Button gQuitButton = Button("Quit", 490,10,600,60,false);
Button gUndoButton = Button("Un-Do", 610, 10, 690, 60, false);
Button gSaveButton = Button("Save", 610, 70, 690, 120, false);
Button gLoadButton = Button("Load", 490, 70, 600, 120, false);
Button gHelpButton = Button("Help", screen_x - 80, screen_y - 60, screen_x - 10, screen_y - 10, false);
// 
// Functions that draw basic primitives
//
void SaveShapes()
{
	ofstream fout("data.txt");
	if(!fout)
	{
		cerr << "error: could not open file to write out your data";
		return;//gets you out of the function
	}
	for(int i = 0; i < gShapes.size(); i++)
	{
		gShapes[i]->Save(fout);
	}
	fout<<"   " <<endl;
}
void DrawCircle(GLdouble x1, GLdouble y1, GLdouble radius)
{
	glBegin(GL_POLYGON);
	for(int i=0; i<32; i++)
	{
		double theta = (double)i/32.0 * 2.0 * 3.1415926;
		double x = x1 + radius * cos(theta);
		double y = y1 + radius * sin(theta);
		glVertex2d(x, y);
	}
	glEnd();
}

void DrawRectangle(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2)
{
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y1);
	glVertex2d(x2,y2);
	glVertex2d(x1,y2);
	glEnd();
}

void DrawTriangle(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2, GLdouble x3, GLdouble y3)
{
	glBegin(GL_TRIANGLES);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glEnd();
}

// Outputs a string of text at the specified location.
void text_output(GLdouble x, GLdouble y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}


//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	if(help)
	{
		glClear(GL_COLOR_BUFFER_BIT);
		text_output(200,250,"may the force be with you!");
		gTempClicksCount = 0;
	}
	else
	{
	glClear(GL_COLOR_BUFFER_BIT);
	gbottom.Draw();
	gRectButton.Draw();
	gCircButton.Draw();
	gTriButton.Draw();
	gClearButton.Draw();
	gQuitButton.Draw();
	gRedSlider.Draw();
	gGreenSlider.Draw();
	gBlueSlider.Draw();
	gColorBox.Draw();
	gRefPoint.Draw();
	gRefPoint1.Draw();
	gSaveButton.Draw();
	gLoadButton.Draw();
	gHelpButton.Draw();
	for(unsigned int i =0; i < gShapes.size(); i++)
	{
		gShapes[i]->Draw();//its a pointer so you need to use ->
		gbottom.Draw();
		gRectButton.Draw();
		gCircButton.Draw();
		gTriButton.Draw();
		gClearButton.Draw();
		gQuitButton.Draw();
		gRedSlider.Draw();
		gGreenSlider.Draw();
		gBlueSlider.Draw();
		gColorBox.Draw();
		gRefPoint.Draw();
		gRefPoint1.Draw();
		gUndoButton.Draw();
		gSaveButton.Draw();
		gLoadButton.Draw();
		gHelpButton.Draw();


	}
	}
	glutSwapBuffers();
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	case 'b':
		// do something when 'b' character is hit.
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	double red = gRedSlider.GetValue();
	double green = gGreenSlider.GetValue();
	double blue = gBlueSlider.GetValue();
	y = screen_y - y;//flip so 0 is bottom left

	if(mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
		helpCount++;
		if(help)
		{
		help = false;
		gTempClicksCount = -1;
		helpCount = 0;
		}
		if(gRectButton.isClicked(x,y))
		{
			gRectButton.SetSelected(true);
			gCircButton.SetSelected(false);
			gTriButton.SetSelected(false);
			gClearButton.SetSelected(false);
			gTempClicksCount = 0;
		}
		else if(gCircButton.isClicked(x,y))
		{
			gRectButton.SetSelected(false);
			gCircButton.SetSelected(true);
			gTriButton.SetSelected(false);
			gClearButton.SetSelected(false);
			gTempClicksCount = 0;
		}
		else if(gTriButton.isClicked(x,y))
		{
			gRectButton.SetSelected(false);
			gCircButton.SetSelected(false);
			gTriButton.SetSelected(true);
			gClearButton.SetSelected(false);
			gTempClicksCount = 0;
		}
		else if(gClearButton.isClicked(x,y))
		{
			for( int i = 0; i<gShapes.size(); i++)
			{
				delete gShapes[i];
			}
			gRefPoint = Rectangle(0,0,0,0,0,0,0);
			gRefPoint1 = Rectangle(0,0,0,0,0,0,0);
			gShapes.clear();


		}
		else if(gQuitButton.isClicked(x,y))
		{
			exit(0);
		}
		else if(gUndoButton.isClicked(x,y))
		{
			int size = gShapes.size();
			gShapes.resize(size -1);
			gRefPoint = Rectangle(0,0,0,0,0,0,0);
			gRefPoint1 = Rectangle(0,0,0,0,0,0,0);
		}
		else if(gHelpButton.isClicked(x,y))
		{
			help = true;
			gTempClicksCount = 0;
			gRefPoint = Rectangle(0,0,2,2,1,1,1);
			gRefPoint1 = Rectangle(0,0,2,2,1,1,1);

		}
		else if(gSaveButton.isClicked(x,y))
		{
			SaveShapes();
		}
		else if(gLoadButton.isClicked(x,y))
		{
			ifstream fin("data.txt");
			if(!fin)
			{
				cerr << "error: could not open file to write out your data";
				return;//gets you out of the function
			}
			while(!fin.eof())//while not at end of file
			{
				char type[20];
				fin >> type;
				if(strcmp(type,"Rectangle")==0)
				{
					double x1, y1, x2, y2, r, g, b;
					fin >> x1 >> y1 >> x2 >> y2 >> r >> g >> b >> ws;//reads everything until whitespace
					Rectangle *tempRect = new Rectangle(x1, y1, x2, y2, r, g, b);
					gShapes.push_back(tempRect);
				}
				else if(strcmp(type,"Circle")==0)
				{
					double x, y, radious, r, g, b;
					fin >> x >> y >> radious >> r >> g >> b >> ws;
					Circle *tempCircle = new Circle(x,y,radious, r, g, b);
					gShapes.push_back(tempCircle);
				}
				else if(strcmp(type,"Triangle")==0)
				{
					double x1, y1, x2, y2, x3, y3, r, g, b;
					fin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> r >> g >> b >> ws;
					Triangle *tempTri = new Triangle(x1, y1, x2, y2, x3, y3, r, g, b);
					gShapes.push_back(tempTri);
				}
			}
		}

		else if(gRedSlider.HandleMouseClick(x,y))
		{

		}
		else if(gGreenSlider.HandleMouseClick(x,y))
		{

		}
		else if(gBlueSlider.HandleMouseClick(x,y))
		{

		}
		else
		{
			//continue making the current shape.
			if(helpCount > 0)
			{
			gRefPoint1 = gRefPoint;
			gRefPoint = Rectangle(x -2, y -2, x + 2, y + 2, .5, .5, .5);
			}
			gTempClicksX[gTempClicksCount] = x;
			gTempClicksY[gTempClicksCount] = y;

			gTempClicksCount ++;
			if(gRectButton.IsSelected() && gTempClicksCount == 2)
			{
				//Make a new Rectangle and reset the TempClicksCount to 0.
				Rectangle *tempRect = new Rectangle(gTempClicksX[0], gTempClicksY[0], gTempClicksX[1], gTempClicksY[1], red, green, blue);

				gShapes.push_back(tempRect);
				gTempClicksCount = 0;
			}
			else if(gTriButton.IsSelected() && gTempClicksCount == 3)
			{

				//Make a new Triangle and reset the TempClicksCount to 0.

				Triangle *tempTri = new Triangle(gTempClicksX[0], gTempClicksY[0], gTempClicksX[1], gTempClicksY[1], gTempClicksX[2], gTempClicksY[2], red, green, blue);

				gShapes.push_back(tempTri);
				gTempClicksCount = 0;
			}
			else if(gCircButton.IsSelected() && gTempClicksCount == 2)
			{
				double radious = sqrt((gTempClicksX[0]-gTempClicksX[1])*(gTempClicksX[0]-gTempClicksX[1])+(gTempClicksY[0]-gTempClicksY[1])*(gTempClicksY[0]-gTempClicksY[1]));
				//Make a new Circle and reset the TempClicksCount to 0.
				Circle *tempCirc = new Circle(gTempClicksX[0], gTempClicksY[0], radious, red, green, blue);

				gShapes.push_back(tempCirc);
				gTempClicksCount = 0;
			}
		}


	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	gColorBox = Rectangle(370,70,480,140,red,green,blue);
	glutPostRedisplay();
}
void motion(int x, int y)
{
	y = screen_y - y;//flip so 0 is bottom left
	if(gRedSlider.HandleMouseClick(x,y))
	{
	}
	else if(gGreenSlider.HandleMouseClick(x,y))
	{
	}
	else if(gBlueSlider.HandleMouseClick(x,y))
	{
	}
	glutPostRedisplay();
}

// Your initialization code goes here.
void InitializeMyStuff()
{


}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Program 1 - Shapes");
	}

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	glColor3d(0,0,0); // forground color
	glClearColor(1, 1, 1, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
