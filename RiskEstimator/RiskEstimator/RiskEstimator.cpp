#include <iostream>;
#include <ctime>;
using namespace std;

int main()
{
	float NumPlays = 0.00;
	float winCount = 0.00;
	float loseCount = 0.00;
	int attackDie = 0;
	int defendDie = 0;
	int a3 = 0;
	int a3d2 = 0;
	int a3d1 = 0;
	int a2 = 0;
	int a2d2 = 0;
	int a2d1 = 0;
	int a1 = 0;
	int a1d2 = 0;
	int a1d1 = 0;
	int exit = 1;

	srand(time(0));
	while(1)
	{
		NumPlays++;
		cout << "how many die will the attacker roll?(please enter a number between 1 and 3)" <<endl;
		cin >> attackDie;
		cout <<"how many die will the defender roll?(please enter a number between 1 and 2)" <<endl;
		cin >> defendDie;
		if(attackDie ==  3)//if the attacker chooses to roll 3 dice
		{

			int x = (rand()%6)+1;
			cout << x <<endl;
			int y = (rand()%6)+1;
			cout << y <<endl;
			int z = (rand()%6)+1;
			cout << z <<endl;
			if(x > y)
			{
				if(y > z)
				{
					a3 = x + y;
				}
				else
				{
					a3 = x + z;
				}
			}else
			{
				if(x > z)
				{
					a3 = y + x;
				}else
				{
					a3 = y + z;
				}

			}


			if(defendDie ==2)//if the defender chooses to roll 2 dice
			{

				int x = (rand()%6)+1;
				cout << x <<endl;
				int y = (rand()%6)+1;
				cout << y <<endl;
				a3d2 = x + y;

				if(a3 > a3d2)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
			else//if the defender rolls 1 die
			{
				int a3d1 = (rand()%6)+1;
				cout << a3d1 <<endl;
				if(a3 > a3d1)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
		}
		if( attackDie ==2)//if the attacker rolls 2 dice
		{
			int x = (rand()%6)+1;
			cout << x <<endl;
			int y = (rand()%6)+1;
			cout << y <<endl;
			a2 = x + y;
			if(defendDie ==2)//if the defender chooses to roll 2 dice
			{

				int x = (rand()%6)+1;
				cout << x <<endl;
				int y = (rand()%6)+1;
				cout << y <<endl;
				a2d2 = x + y;

				if(a2 > a2d2)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
			else//if the defender rolls 1 die
			{
				int a2d1 = (rand()%6)+1;
				cout << a2d1 <<endl;
				if(a2 > a2d1)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
		}
		if(attackDie == 1)//if the attacker rolls 1 die
		{
			int a1 = (rand()%6)+1;
			cout << a1 << endl;
			if(defendDie ==2)//if the defender chooses to roll 2 dice
			{

				int x = (rand()%6)+1;
				cout << x <<endl;
				int y = (rand()%6)+1;
				cout << y <<endl;
				a1d2 = x + y;

				if(a1 > a1d2)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
			else//if the defender rolls 1 die
			{
				int a1d1 = (rand()%6)+1;
				cout << a1d1 <<endl;
				if(a1 > a1d1)
				{
					cout << "Attacker wins!" <<endl;
					winCount++;
				}else
				{
					cout << "Attacker loses" <<endl;
					loseCount++;
				}
			}
		}
		cout << "out of " << NumPlays << " Battles, here are the results." <<endl;
		cout << "Number of wins " << winCount << endl;
		cout << "Number of loses " << loseCount << endl;
		cout << "Winning percentage: " << (winCount/NumPlays)*100 << "%" <<endl;
		cout << "To exit enter '0' to continue enter '1'" <<endl;
		cin >> exit;
		if(exit == 0){
			return 0;
		}
	}
	system("PAUSE");
	return 0;
}