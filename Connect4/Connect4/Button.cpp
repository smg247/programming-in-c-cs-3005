#include <iostream>
using namespace std;
#include "Button.h"
#include "glut.h"
#include "Graphics.h"
Button::Button(char label[], int x1, int y1, int x2,int y2, bool selected)
{
	strcpy(mLabel, label);
	mX1 = x1;
	mY1 = y1;
	mX2 = x2;
	mY2 = y2;
	mSelected = selected;
}
bool Button::IsSelected()
{
	if(mSelected)
		return true;
	else
		return false;
}
bool Button::isClicked(int x, int y)
{
	if( x >=mX1 && x <= mX2 && y >= mY1 && y <= mY2)
		return true;
	else
		return false;
}
void Button::SetSelected(bool selected)
{
	mSelected = selected;
}
void Button::Draw()
{
	glColor3d(1,1,.5);
	DrawRectangle(mX1, mY1, mX2, mY2);
	if(mSelected)
		glColor3d(1,0,0);
	else
		glColor3d(.5,.5,.5);
	text_output(mX1+15, mY1+20, mLabel);
	
	//get the text inside the button, and change the color if its selected
}