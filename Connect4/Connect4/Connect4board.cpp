#include <iostream>
#include <vector>
using namespace std;
#include "board.h"
#include "glut.h"
#include "Graphics.h"
#include "Shapes.h"
double xLoc1, yLoc1, xLoc2, yLoc2,xLoc3,yLoc3, xLoc4, yLoc4;
Rectangle rect = Rectangle(0,0,0,0,0,0,0,0,.5,.5,.5);
Rectangle rectDraw = Rectangle(0,0,0,0,0,0,0,0,.5,.5,.5);
Circle circ;
Rectangle gRect[8];

int first;
int second;
int checkTurn;
int moveCount=0;
Connect4Board::Connect4Board()
{
	for(int i = 0; i < 7; i++)
	{
		for(int j = 0; j < 8;j++)
		{
			pieces[i][j] = 0;
		}
	}
	turn = 1;
}
int Connect4Board::GameOver()//probably could have done this with less if statements, but it does over all possible situations this way
{
	bool flag = true;
	bool checkTie = true;
	if(turn == 1)//flips the turn back for testing without effecting the turn
		checkTurn = 2;
	else if(turn == 2)
	{
		checkTurn = 1;
	}
	//pieces[first][second] should equal checkTurn at this point no matter what!
	if(pieces[first-1][second] == checkTurn && pieces[first+1][second] == checkTurn && pieces[first+2][second] == checkTurn)//if it isn't the end case up
	{
		return checkTurn;
	}
	if(pieces[first+1][second] == checkTurn && pieces[first-1][second] == checkTurn && pieces[first-2][second] == checkTurn)//if it isn't the end case up
	{
		return checkTurn;
	}
	if(pieces[first][second -1] == checkTurn && pieces[first][second + 1] == checkTurn && pieces[first][second + 2] == checkTurn)//if it isn't the end case right
	{
		return checkTurn;
	}
	if(pieces[first][second +1] == checkTurn && pieces[first][second - 1] == checkTurn && pieces[first][second - 2] == checkTurn)//if it isn't the end case left
	{
		return checkTurn;
	}
	if(pieces[first-1][second -1] == checkTurn && pieces[first+1][second + 1] == checkTurn && pieces[first+2][second + 2] == checkTurn && second)//if it isnt the end case diag up right
	{
		return checkTurn;
	}
	if(pieces[first+1][second +1] == checkTurn && pieces[first-1][second - 1] == checkTurn && pieces[first-2][second - 2] == checkTurn)//if it isnt the end case diag down left
	{
		return checkTurn;
	}
	if(pieces[first+1][second -1] == checkTurn && pieces[first-1][second + 1] == checkTurn && pieces[first-2][second + 2] == checkTurn && second)//if it isnt the end case diag down right
	{
		return checkTurn;
	}
	if(pieces[first-1][second +1] == checkTurn && pieces[first+1][second - 1] == checkTurn && pieces[first+2][second - 2] == checkTurn)//if it isnt the end case diag up left
	{
		return checkTurn;
	}

	if(pieces[first-1][second] ==checkTurn)//takes care of down wins
	{
		if(pieces[first-2][second] == checkTurn)
		{
			if(pieces[first-3][second] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(pieces[first+1][second] ==checkTurn)//takes care of up wins
	{
		if(pieces[first+2][second] == checkTurn)
		{
			if(pieces[first+3][second] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(pieces[first][second+1] == checkTurn)//takes care of wins to the left
	{
		if(pieces[first][second+2] == checkTurn)
		{
			if(pieces[first][second+3] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(pieces[first][second-1] == checkTurn)//takes care of wins to the right
	{
		if(pieces[first][second-2] == checkTurn)
		{
			if(pieces[first][second-3] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(first > 2)
	{
		if(pieces[first-1][second-1] == checkTurn)//takes care of wins to the diag to the left down
		{
			if(pieces[first-2][second-2] == checkTurn)
			{
				if(pieces[first-3][second-3] == checkTurn)
				{
					return checkTurn;
				}
			}
		}
		if(pieces[first-1][second+1] == checkTurn)//takes care of wins to the diag to the right down
		{
			if(pieces[first-2][second+2] == checkTurn)
			{
				if(pieces[first-3][second+3] == checkTurn)
				{
					return checkTurn;
				}
			}
		}
	}
	if(pieces[first+1][second-1] == checkTurn)//takes care of wins to the diag to the left up
	{
		if(pieces[first+2][second-2] == checkTurn)
		{
			if(pieces[first+3][second-3] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(pieces[first+1][second+1] == checkTurn)//takes care of wins to the diag to the right up
	{
		if(pieces[first+2][second+2] == checkTurn)
		{
			if(pieces[first+3][second+3] == checkTurn)
			{
				return checkTurn;
			}
		}
	}
	if(checkTie)//takes care of the game still going or ties
	{
		if(pieces[5][0] && pieces[5][1] != 0 && pieces[5][2] !=0 && pieces[5][3] != 0 && pieces[5][4] != 0 && pieces[5][5] !=0 && pieces[5][6] != 0)
		{
			return 3;
		}
		else
		{
			return 0;
		}
	}
}
bool Connect4Board::Draw()
{
	double x1 = 10;
	double y1 = 10;
	double x2 = 70;
	double y2 = 10;
	double x3 = 70;
	double y3 = 70;
	double x4 = 10;
	double y4 = 70;

	for(int i = 0; i < 6; i++)
	{
		for(int j = 0; j < 7;j++)
		{
			if(pieces[i][j]==0)
			{
				//rect = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,1,1,1);
				circ = Circle(x1+30,y1+30,29,1,1,1);
			}
			else if(pieces[i][j]==1)
			{
				//rect = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,1,0,0);
				circ = Circle(x1+30,y1+30,29,1,0,0);
			}
			else if(pieces[i][j]==2)
			{
				//rect = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,0,0,0);
				circ = Circle(x1+30,y1+30,29,0,0,0);
			}
			rect = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,1,1,0);
			rect.Draw();
			circ.Draw();
			x1+=60;
			x2+=60;
			x3+=60;
			x4+=60;

		}
		x1=10;
		x2=70;
		x3=70;
		x4=10;
		y1+=60;
		y2+=60;
		y3+=60;
		y4+=60;
	}
	y1+=20;
	y2 +=20;
	y3 +=20;
	y4+=20;
	for(int k =0; k < 7; k++)
	{

		if(turn == 1)
		{

			rectDraw = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,1,0,0);
		}
		else if(turn == 2)
		{

			rectDraw = Rectangle(x1,y1,x2,y2,x3,y3,x4,y4,0,0,0);
		}
		x1 +=60;
		x2 +=60;
		x3 +=60;
		x4 +=60;
		gRect[k] = rectDraw;
		rectDraw.Draw();


	}

	if(turn == 1)
	{
		glColor3f (1, 0, 0);
		text_output(x1+10,y1+50,"It is Red's turn!");
	}else{
		glColor3f(0, 0, 0);
		text_output(x1+10,y1+50,"It is Black's turn!");
	}
	xLoc1 = x1;
	yLoc1 = y1;
	xLoc2 = x2;
	yLoc2 = y2;
	xLoc3 = x3;
	yLoc3 = y3;
	xLoc4 = x4;
	yLoc4 = y4;
	return true;
}
void Connect4Board::HandleMouseClick(int x, int y)
{
	for(int i = 0; i < 7; i++)//puts the current color on the correct spot
	{
		if(gRect[i].isClicked(x,y))
		{
			for(int j = 0; j < 6; j++)
			{
				if(pieces[j][i] == 0)//first up and down, second left and right
				{
					pieces[j][i]= turn;
					first = j;
					second = i;
					moveCount++;
					if(turn == 1)//logic to cycle turns
						turn =2;
					else
						turn =1;

					break;
				}

			}

		}

	}
}
