#include <fstream>
using namespace std;
#include "Graphics.h"
class Shape
{
public:
	Shape();
	virtual void Draw()=0;// makes Draw() a pure virtual method. meaning that classes that derive from Shapes, must provide their own Draw().
	virtual void Save(ofstream& fout)=0;
protected://means that derived classes can see this data.
	double mR, mG, mB;
};
class Rectangle : public Shape
{
public:
	Rectangle(double mX1, double mY1, double mX2, double mY2, double mX3, double mY3, double mX4, double mY4, double mR, double mG, double mB);
	Rectangle();
	void Draw();
	void Save(ofstream& fout);
	bool isClicked(int x, int y);
private:
	double mX1, mY1, mX2, mY2, mX3, mY3, mX4, mY4;
	
};
class Circle : public Shape
{
public:
	Circle(double mX, double mY, double mRadious, double mR, double mG, double mB);
	Circle();
	void Draw();
	void Save(ofstream& fout);
private:
	double mX, mY, mRadious;
};

