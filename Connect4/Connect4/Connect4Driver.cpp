#include <cmath>
#include <cstring>
#include <iostream>
#include <dos.h>
#include <windows.h>
using namespace std;
#include "glut.h"
#include "Graphics.h"
#include "Shapes.h"
#include "board.h"
// Global Variables (Only what you need!)
GLdouble screen_x = 700;
GLdouble screen_y = 500;
Connect4Board board = Connect4Board();
int whoWon = 0;
Circle cursor = Circle(0,0,30,0,0,0);
//Rectangle rect = Rectangle(10,10,20,20,.5,.5,.5);
// 
// Functions that draw basic primitives
//
int turn = board.turn;
void DrawCircle(double x1, double y1, double radius)
{
	glBegin(GL_POLYGON);
	for(int i=0; i<32; i++)
	{
		double theta = (double)i/32.0 * 2.0 * 3.1415926;
		double x = x1 + radius * cos(theta);
		double y = y1 + radius * sin(theta);
		glVertex2d(x, y);
	}
	glEnd();
}

void DrawRectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)//top left, top right, bottom right, bottom left
{
	glBegin(GL_QUADS);
	glVertex2d(x1,y1);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glVertex2d(x4,y4);
	glEnd();
	glColor3f (1, 1, 0);
	glBegin(GL_LINES);
	glVertex2d(x1, y1);             
	glVertex2d(x2, y2);
	glVertex2d(x2,y2);
	glVertex2d(x3,y3);
	glVertex2d(x3,y3);
	glVertex2d(x4,y4);
	glVertex2d(x4,y4);
	glVertex2d(x1, y1);                 
	glEnd();    
}


// Outputs a string of text at the specified location.
void text_output(double x, double y, char *string)
{
	void *font = GLUT_BITMAP_9_BY_15;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int len, i;
	glRasterPos2d(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++) 
	{
		glutBitmapCharacter(font, string[i]);
	}

	glDisable(GL_BLEND);
}


//
// GLUT callback functions
//

// This callback function gets called by the Glut
// system whenever it decides things need to be redrawn.
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	board.Draw();
	if(whoWon != 0)
	{
		if (whoWon == 1)
		{
			glColor3f (1, 0, 0);
			//glClear(GL_COLOR_BUFFER_BIT);
			text_output(450,300, "Red Wins!");
			
		}
		else if(whoWon == 2)
		{
			glColor3f (0, 0, 0);
			//glClear(GL_COLOR_BUFFER_BIT);
			text_output(450,300, "Black Wins!");
		}
		else if(whoWon == 3)
		{
			//glClear(GL_COLOR_BUFFER_BIT);
			text_output(450,300, "Tie!");
		}
		text_output(450,285,"click to reset");
	}
	glutSwapBuffers();
			
}


// This callback function gets called by the Glut
// system whenever a key is pressed.
void keyboard(unsigned char c, int x, int y)
{
	switch (c) 
	{
	case 27: // escape character means to quit the program
		exit(0);
		break;
	case 'b':
		// do something when 'b' character is hit.
		break;
	default:
		return; // if we don't care, return without glutPostRedisplay()
	}

	glutPostRedisplay();
}


// This callback function gets called by the Glut
// system whenever the window is resized by the user.
void reshape(int w, int h)
{
	// Reset our global variables to the new width and height.
	screen_x = w;
	screen_y = h;

	// Set the pixel resolution of the final picture (Screen coordinates).
	glViewport(0, 0, w, h);

	// Set the projection mode to 2D orthographic, and set the world coordinates:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);
	glMatrixMode(GL_MODELVIEW);

}

// This callback function gets called by the Glut
// system whenever any mouse button goes up or down.
void mouse(int mouse_button, int state, int x, int y)
{
	//y = screen_y - y;//flip so 0 is bottom left

	if(mouse_button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
	{
		if(whoWon == 0)
			board.HandleMouseClick(x,y);
		else
		{
			Sleep(5);
			board = Connect4Board();
			whoWon = 0;
		}
			if(board.GameOver() == 1)//red win
			{

				whoWon = 1;
			}
			if(board.GameOver() == 2)//black win
			{
				//text_output(150,10, "black Wins!");
				whoWon = 2;
			}
			else if(board.GameOver() == 3)//tie
			{
				//text_output(150,10,"Tie!");
				whoWon = 3;
			}
		
	}
	if (mouse_button == GLUT_LEFT_BUTTON && state == GLUT_UP) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) 
	{
	}
	if (mouse_button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) 
	{
	}
	glutPostRedisplay();
}
void glutPassiveMotionFunc(int x, int y)
{
	if (turn == 1)
	{
		cursor = Circle(x,y,30,1,0,0);
	}
	else
	{
		cursor = Circle(x,y,30,0,0,0);
	}
	glutPostRedisplay();
}
// Your initialization code goes here.
void InitializeMyStuff()
{


}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(screen_x, screen_y);
	glutInitWindowPosition(50, 50);

	int fullscreen = 0;
	if (fullscreen) 
	{
		glutGameModeString("800x600:32");
		glutEnterGameMode();
	} 
	else 
	{
		glutCreateWindow("Connect 4");
	}
	//glutSetCursor(GLUT_CURSOR_CYCLE);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glColor3d(0,0,0); // forground color
	glClearColor(1, 1, 1, 0); // background color
	InitializeMyStuff();

	glutMainLoop();

	return 0;
}
