#include <iostream>
#include <vector>
using namespace std;
//this program stores user input numbers in a vector and then prints the smallest, largest, and average of the numbers.
int main()
{
	vector<int> stats;
	int N = 1;
	int min = 1000;
	int max = 0;
	int total = 0;
	int avg = 0;
	cout << "enter a series of numbers to store in the vector, ending with 0" << endl;
	
	while(N != 0)
	{
		cin >> N;
		if(N != 0)
			stats.push_back(N);
	}
	for(int i = 0; i < stats.size(); i++)
	{
		if (stats[i] < min)
			min = stats[i];
		if(stats[i] > max)
			max = stats[i];
		total = total + stats[i];
	}
	avg = total/stats.size();
	cout << min << " is the min" << endl;
	cout << max << " is the max" << endl;
	cout << avg << " is the avg" << endl;
	system("PAUSE");
	return 0;

}