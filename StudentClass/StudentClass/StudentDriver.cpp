/*
Stephen Goeddel
9/27/11
*/
#include <iostream>
#include <cstring>
using namespace std;
#include "Student.h"



int main()
{
	Student s;
	int age = 20;
	char Last[20] = "Goeddel";
	char First[20] = "Stephen";
	char SSN[12] = "123-34-6778";
	char Email[35] = "me@yourhouse.org"; 
	//cout << "Age Please.." << endl;
	//cin >> age;
	//cout << "Last name..." << endl;
	//cin >> Last;
	//cout << "First name..." << endl;
	//cin >> First;
	//cout << "SSN..." << endl;
	//cin >> SSN;
	//cout << "Email..." << endl;
	//cin >> Email;
	s.SetAge(age);
	s.SetLast(Last);
	s.SetFirst(First);
	s.SetSSN(SSN);
	s.SetEmail(Email);
	//cout << "your age is now " << s.GetAge() <<endl;
	//s.GetLast(Last);
	//cout << "your Last name is now " << Last << endl;
	//s.GetFirst(First);
	//cout << "your First name is now " << First << endl;
	//s.GetSSN(SSN);
	//cout << "your SSN is now " << SSN << endl;
	//s.GetEmail(Email);
	//cout << "your Email is now " << Email << endl;
	cout << s;
	//test case that chains, one that does s3 = s3, cout's, and the rest of the recently implemented stuff, test all paths
	Student s1 = s;
	//cout << s1;
	Student s2 = s1 = s;
	//cout << s2;
	s2.SetLast(NULL);
	//cout << s2;
	Student s3 = s3;
	s3.SetSSN("233-45-9887");
	//cout << s3;
	Student s4;
	s4 = s2;
	//if(s4 == s2)
		//cout << "yay!" <<endl;
	if(s3 > s4)
		cout << "yay!" << endl;
	//cout << s4;
	Student *sp = new Student();
	cout << *sp;
	system("PAUSE");
	return 0;
}