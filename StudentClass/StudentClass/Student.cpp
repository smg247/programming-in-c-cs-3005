/*
Stephen Goeddel
9/27/11
*/
#include <iostream>
#include <cstring>
#include <assert.h>
using namespace std;
#include "Student.h"

void Student::SetAge(int age)//ties the method to the Student class's SetAge method
{
	mAge = age;
}
int Student::GetAge() const
{
	return mAge;
}
void Student::SetLast(const char *L)
{
	if (mLast != NULL)
	{
		delete [] mLast;
		mLast = NULL;
	}
	if (L != NULL)
	{
		mLast = new char[strlen(L)+1];
		strcpy(mLast, L);
	}
}
bool Student::GetLast(char *L) const
{
	assert(L != NULL);
	if (mLast != NULL)
	{
		strcpy(L, mLast);
		return true;
	}
	else
	{
		return false;
	}
}
void Student::SetFirst(const char *F)
{
	if (mFirst != NULL)
	{
		delete [] mFirst;
		mFirst = NULL;
	}
	if (F != NULL)
	{
		mFirst = new char[strlen(F)+1];
		strcpy(mFirst, F);
	}
}
bool Student::GetFirst(char *F) const
{
	assert(F != NULL);
	if (mFirst != NULL)
	{
		strcpy(F, mFirst);
		return true;
	}
	else
	{
		return false;
	}
}
void Student::SetSSN(const char *S)
{
	assert(*S != NULL);
	strcpy(mSSN, S);
}
void Student::GetSSN(char *S) const
{
	strcpy(S, mSSN);
}
void Student::SetEmail(const char *E)
{
	if (mEmail != NULL)
	{
		delete [] mEmail;
		mEmail = NULL;
	}
	if (E != NULL)
	{
		mEmail = new char[strlen(E)+1];
		strcpy(mEmail, E);
	}
}
bool Student::GetEmail(char *E) const
{
	assert(E != NULL);
	if (mEmail != NULL)
	{
		strcpy(E, mEmail);
		return true;
	}
	else
	{
		return false;
	}
}
void Student::Initialize()
{
	mAge = 0;
	mLast = NULL;
	mFirst = NULL;
	mEmail = NULL;
	strcpy(mSSN, "");
}
Student::Student()//initialize all class variables in here
{
	Initialize();
}
Student::~Student()
{
	if(mLast != NULL)
		delete [] mLast;
	if(mFirst != NULL)
		delete [] mFirst;
	if(mEmail != NULL)
		delete [] mEmail;
}
Student::Student(const Student &rhs)//this points to new object  rhs = right hand side, or old object
{
	Initialize();
	*this = rhs;
}
const Student& Student::operator=(const Student &rhs)
{
	if (this == &rhs)//avoid bugs involving s3 = s3; &rhs means the address of the rhs object
	{
		return *this;
	}
	else
	{
		this ->SetAge(rhs.GetAge());

		strcpy(this -> mSSN, rhs.mSSN);

		char buffer[100];
		bool Lastok = rhs.GetLast(buffer);
		if (Lastok)
		{
			this -> SetLast(buffer);
		}
		else
		{
			this -> SetLast(NULL);
		}

		bool Firstok = rhs.GetFirst(buffer);
		if (Firstok)
		{
			this -> SetFirst(buffer);
		}
		else
		{
			this -> SetFirst(NULL);
		}

		bool Emailok = rhs.GetEmail(buffer);
		if (Emailok)
		{
			this -> SetEmail(buffer);
		}
		else
		{
			this -> SetEmail(NULL);
		}

		return *this;//returns a pointer to the new student
	}
}
bool Student::operator==(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)==0;
}
bool Student::operator<(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)<0;
}
bool Student::operator>(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)>0;
}
bool Student::operator!=(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)!=0;
}
bool Student::operator<=(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)<=0;
}
bool Student::operator>=(const Student &rhs) const
{
	//compare the key field (mSSN)
	return strcmp(this->mSSN, rhs.mSSN)>=0;
}
ostream & operator<<(ostream &out, const Student &rhs)
{
	char buffer[100];
	bool ok = rhs.GetLast(buffer);
	if(ok)
		out << buffer << " ";
	else
		out << "(no last name)";

	char buffer1[100];
	bool ok1 = rhs.GetFirst(buffer1);
	if(ok1)
		out << buffer1 << " ";
	else
		out << "(no first name)";

	char buffer2[100];
	bool ok2 = rhs.GetEmail(buffer2);
	if(ok2)
		out << buffer2 << " ";
	else
		out << "(no Email)";
	
	out << rhs.GetAge() << " ";

	char SSN[12];
	rhs.GetSSN(SSN);
	if(SSN == "")
	{
		out << "(no SSN)";
	}
	else
	{
		out << SSN << " ";
	}
	return out;
}