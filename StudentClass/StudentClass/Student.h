/*
Stephen Goeddel
9/27/11
*/
class Student
{
private://all following variables are private
	int mAge;//its a member of this class not a global variable
	char *mLast;
	char *mFirst;
	char mSSN[12]; //mSSN can never be NULL
	char *mEmail;
public://all following variables are now public
	Student();//default constructor
	~Student();//destructor
	Student(const Student &rhs);//copy constructor---can't change the student through this handle
	const Student & operator=( const Student &rhs);//overrides assignment operator, returns a Student reference to support code such as 's4 = s5 = s1;'

	bool operator==(const Student &rhs) const;//operational methods
	bool operator<(const Student &rhs) const;
	bool operator>(const Student &rhs) const;
	bool operator!=(const Student &rhs) const;
	bool operator<=(const Student &rhs) const;
	bool operator>=(const Student &rhs) const;

	void Initialize();//called at the beggining of every constructor
	void SetAge(int age);
	int GetAge() const;
	void SetLast(const char * L);//says "I wont dereference this pointer and change the original value
	bool GetLast(char * L) const;//returns false if mLast is NULL and the method won't change any of the data of the class because of 'const'
	void SetFirst(const char * F);
	bool GetFirst(char * F) const;//same as above
	void SetSSN(const char * S);
	void GetSSN(char * S) const;
	void SetEmail(const char * E);
	bool GetEmail(char * E) const;//same as above

};//don't forget the ending semicolon in a class
//stand alone functions that need to be available
ostream & operator<<(ostream &out, const Student &rhs);