#include <iostream>;
#include <ctime>;
using namespace std;

int main()
{
	float winCount = 0.00;
	float lossCount = 0.00;
	float playCount = 0.00;
	int firstDie = 0;
	int secondDie = 0;
	int firstRoll = 0;
	int rolls = 0;
	int point = 0;
	bool goOn = true;
	srand(time(0));

	
	for(int i = 0; i < 1000000; i++)
	{
		goOn = true;
		firstDie = (rand()%6)+1;
		secondDie = (rand()%6)+1;
		firstRoll = firstDie + secondDie;
		//cout << "your first roll was: " << firstRoll << endl;
		if(firstRoll == 7 || firstRoll == 11)
		{
			winCount ++;
		}
		else if(firstRoll == 2 || firstRoll == 3 || firstRoll == 12)
		{
			lossCount ++;
		}
		else
		{
			point = firstRoll;
			while(goOn == true)
			{
				firstDie = (rand()%6)+1;
				secondDie = (rand()%6)+1;
				rolls = firstDie + secondDie;
				if(rolls == point)
				{
					winCount++;
					//cout << rolls << endl;
					goOn = false;
				}
				else if(rolls == 7)
				{
					lossCount ++;
					//cout << rolls << endl;
					goOn = false;
				}
				else
				{
					//cout << "the game goes on!" << endl;
					goOn = true;
				}
			}


		}
		playCount++;
	}
	cout << "your total number of wins was: " << winCount << endl;
	cout << "your total number of losses was: " << lossCount << endl;
	cout << "your total number of plays was: " << playCount << endl;
	cout << "Winning percentage: " << (winCount/playCount)*100 << "%" <<endl;
	system("PAUSE");
	return 0;
}