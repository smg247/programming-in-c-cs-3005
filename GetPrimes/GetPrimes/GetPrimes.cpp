#include <iostream>;
#include <cmath>;
using namespace std;
//this program asks the user to input a from number and a to number, and prints and counts all the primes.
bool IsPrime(int x){
	if(x == 2){
		return true;
	}
	if(x%2 == 0 || x == 1){
		return false;
	}
	double r = sqrt((double)x);//cast the int x as a double using the parenthesis, turns x into the square root of x and puts it into double r
	for(int i =3; i <= ceil(r); i+=2){//takes the ceiling of r, or rounds up
		if( x%i == 0){
			return false;
		}
	}
	return true;
}
int main(){
	int from = 0;
	int to = 0;
	int count = 0;
	cout << "enter the number you would like to find the primes to:  ";
	cin >> to;
	cout << "enter the number you would like to find the primes from: ";
	cin >> from;
		for( int i=from; i<to+1; i+=1 )
		{
			if ( IsPrime(i) ) 
			{
				count += 1; 
			cout << i << endl;
			}
		}
	cout << "There are " << count << " primes between " <<from <<" and " << to << endl;
	system("PAUSE");
	return 0;
}