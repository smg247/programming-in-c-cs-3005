//Stephen Goeddel
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int FindSmallest(int A[], int size)
{
	int s = A[0];
	for(int i = 1; i < size; i++)
	{
		if(A[i] < s)
		{
			s = A[i];
		}

	}
	return s;
}
int LineSearch(int A[], int size, int find)
{
	for(int i = 0; i < size; i++)
	{
		if (A[i] == find)
		{
			return i;
		}
	}
	return -1;
}
void Sort(int A[], int size)//bubble sort
{
	bool swapped = true;
	int lower;
	int j = 0;
	while(swapped)
	{
		swapped = false;
		j++;
		for(int i = 0; i < size - j; i++)
		{
			
			if(A[i] > A[i + 1])
			{
				lower = A[i];
				A[i] = A[i + 1];
				A[i + 1] = lower;

				swapped = true;
				
			}
			
		}
		
	}
	for(int k = 0; k < size; k++) 
		{
		   cout << A[k] << endl;;
		}
}
int BinSearch(int A[], int size, int find)
{
	int low = 0;
	int high = size -1;
	while(low <= high)
	{
		int mid = high - (high - low)/2;
		if(A[mid] == find)
		{
			return mid;
			
		}
		else if(A[mid] < find)
		{
			low = mid + 1;
		}else
		{
			high = mid - 1;
		}
	}
	return -1;


}
int main()
{
	srand((unsigned int)time(0));
	const int size = 10;
	int A[size];
	for(int i = 0; i < size; i++)
	{
		A[i] = rand()%100;
		cout << A[i] << endl;
	}
	int s = FindSmallest(A, size);
	int f = LineSearch(A, size, 17);
	int f1 = LineSearch(A, size, 25);
	int f2 = LineSearch(A, size, 32);

	cout <<"The smallest number is:  "<< s << endl;
	cout <<"17, according to linear search, is located in index:  " << f << endl;
	cout <<"25, according to linear search, is located in index:  " << f1 << endl;
	cout <<"32, according to linear search, is located in index:  " << f2 << endl;
	
	int f3 = BinSearch(A, size, 22); 
	int f4 = BinSearch(A, size, 5); 
	int f5 = BinSearch(A, size, 61); 
	cout <<"22, according to Binary Search, is located in index:  " << f3 << endl;
	cout <<"5, according to Binary Search, is located in index:  " << f4 << endl;
	cout <<"61, according to Binary Search, is located in index:  " << f5 << endl;
	cout <<"The sorted array follows," << endl;
	Sort(A, size);
	system("PAUSE");
	return 0;

}